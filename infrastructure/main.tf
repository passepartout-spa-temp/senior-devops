terraform {
  backend "s3" {
    bucket         = "passepartout-terraform"
    key            = "project.tfstate"
    region         = "eu-west-1"
    dynamodb_table = "passepartout-terraform"
    profile        = "default"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "= 3.63.0"
    }

    archive = {
      source  = "hashicorp/archive"
      version = "= 2.2.0"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}


module "infrastructure" {
  source = "./modules"
  
  # Parametri fissi
  prefix = "recruitment"
  ami_id = "ami-0d7f394c51e3e7d3e"
  vpc_id = "vpc-54843e2d"
  instance_type = "t3.micro"
  subnet_id = "subnet-f29d6a8b"
}
