# Buckets per gli artifacts del sito
resource "aws_s3_bucket" "artifacts_bucket" {
  bucket = "${var.prefix}-artifacts"
  acl    = "private"
  versioning {
    enabled = false
  }
}
resource "aws_s3_bucket_public_access_block" "S3_access_artifacts" {
  bucket                  = aws_s3_bucket.artifacts_bucket.id
  block_public_acls       = true
  block_public_policy     = true
  restrict_public_buckets = true
  ignore_public_acls      = true
}

# Bucket S3 per gli eventi di CloudTrail
resource "aws_s3_bucket" "bucket_trail" {
  bucket        = "${var.prefix}-trail-logs"
  force_destroy = true
}

resource "aws_s3_bucket_public_access_block" "S3_bucket_trail" {
  bucket                  = aws_s3_bucket.bucket_trail.id
  block_public_acls       = true
  block_public_policy     = true
  restrict_public_buckets = true
  ignore_public_acls      = true
}


