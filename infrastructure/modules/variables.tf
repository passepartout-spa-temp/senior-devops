variable "prefix" {
  description = "Prefisso per le risorse create"
  type = string
}

variable "ami_id" {
  description = "ID dell'AMI con cui costruire l'istanza"
  type = string
}

variable "instance_type" {
  description = "Tipo istanza"
  type = string
}

variable "vpc_id" {
  description = "ID del VPC"
  type = string
}

variable "subnet_id" {
  description = "ID Subnet"
  type = string
}