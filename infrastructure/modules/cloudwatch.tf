# Rule to trigger site deploy
resource "aws_cloudwatch_event_rule" "rule_deploy" {
  name = "new-site-deployment"
  description = "New version of site.zip uploaded to S3 bucket"

  event_pattern = <<EOF
{
    "source": [
        "aws.s3"
    ],
    "detail-type": [
        "AWS API Call via CloudTrail"
    ],
    "detail": {
        "eventSource": [
            "s3.amazonaws.com"
        ],
        "eventName": [
            "CopyObject",
            "CompleteMultipartUpload",
            "PutObject"
        ],
        "requestParameters": {
            "bucketName": [
                "passepartout-terraform"
            ],
            "key": [
                "site.zip"
            ]
        }
    }
}
EOF
}