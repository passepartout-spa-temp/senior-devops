import boto3

ec2 = boto3.client('ec2', 'eu-west-1')
ssm_client = boto3.client('ssm')

def lambda_handler(event, context):
    try:
        response = ssm_client.send_command(Targets=[{
            'Key': 'tag:Service',
            'Values': [
                'sito-web',
            ]
        }], 
        DocumentName='AWS-RunShellScript',
        Parameters={
            "commands": [
                "sudo apt update -y",
                "sudo apt install nginx unzip -y",
                "cd && curl 'https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip' -o 'awscliv2.zip'",
                "unzip awscliv2.zip",
                "sudo ./aws/install",
                "aws s3 cp s3://recruitment-artifacts/site.zip /home/site.zip",
                "sudo unzip -o /home/site.zip -d /var/www/html/"
            ]
        })
        print(str(response))
    except Exception as e:
        print("Errore " + str(e))
        raise

#lambda_handler(None, None)