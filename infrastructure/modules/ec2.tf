resource "aws_iam_instance_profile" "ec2_role_profile" {
  name = "${var.prefix}_Role_EC2"
  role = aws_iam_role.role_ec2.name
}

resource "aws_iam_role" "role_ec2" {
  name = "${var.prefix}_Role_EC2"
  path = "/"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid    = "",
        Effect = "Allow",
        Principal = {
          Service = [
            "ec2.amazonaws.com"
          ]
        },
        Action = "sts:AssumeRole"
      }
    ]
  })
}

resource "aws_iam_policy" "policy_ec2" {
  name        = "${var.prefix}_Policy_EC2"
  path        = "/"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "s3:*"
        ]
        Effect   = "Allow"
        Resource = "*"
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "attach_policy_to_role_ec2" {
  role       = aws_iam_role.role_ec2.name
  policy_arn = aws_iam_policy.policy_ec2.arn
}

resource "aws_iam_role_policy_attachment" "attach_policy_to_role_ec2_2" {
  role       = aws_iam_role.role_ec2.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}


resource "aws_security_group" "basic" {
  name        = "${var.prefix}-SG"
  vpc_id      = var.vpc_id
}


resource "aws_security_group_rule" "rule_egress" {
  type              = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.basic.id
}


resource "aws_instance" "web" {
  ami                   = var.ami_id
  instance_type         = var.instance_type
  subnet_id             = var.subnet_id
  iam_instance_profile  = aws_iam_role.role_ec2.name
  key_name              = "key-test"
  vpc_security_group_ids = [aws_security_group.basic.id]
  tags = {
    Name = "website"
    Service = "sito-web"
  }
}

resource "aws_eip" "eip" {
  vpc = true
  instance                  = aws_instance.web.id
  associate_with_private_ip = aws_instance.web.private_ip
}