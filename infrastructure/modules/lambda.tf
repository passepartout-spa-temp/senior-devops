resource "aws_iam_policy" "policy_lambda_deploy" {
  name        = "${var.prefix}_Policy_Deploy"
  path        = "/"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ssm:ListCommands",
          "ssm:ListCommandInvocations",
          "ec2:Describe*"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action = [
          "ssm:SendCommand"
        ]
        Effect = "Allow"
        Resource = [
          "arn:aws:ec2:eu-west-1:${data.aws_caller_identity.current.account_id}:instance/*",
          "arn:aws:ssm:eu-west-1:*:document/AWS-RunShellScript"
        ]
      }
    ]
  })
}

resource "aws_iam_role" "role_lambda_deploy" {
  name        = "${var.prefix}_Role_Deploy"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid    = "",
        Effect = "Allow",
        Principal = {
          Service = [
            "lambda.amazonaws.com"
          ]
        },
        Action = "sts:AssumeRole"
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "attach_policy_to_role_lambda" {
  role       = aws_iam_role.role_lambda_deploy.name
  policy_arn = aws_iam_policy.policy_lambda_deploy.arn
}



resource "aws_lambda_permission" "allow_cloudwatch_to_call_lambda" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_deploy.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.rule_deploy.arn
}

resource "aws_iam_role_policy_attachment" "policy_attach_cloudwatch" {
  role       = aws_iam_role.role_lambda_deploy.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_lambda_function" "lambda_deploy" {
  filename ="lambda_function_payload.zip"
  function_name = "Deploy website"
  role = aws_iam_role.role_lambda_deploy.arn
  handler = "lambda_deploy.py"
}
